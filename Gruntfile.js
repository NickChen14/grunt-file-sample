module.exports = function(grunt) {
    // 下載package.json中的想用套件
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-real-favicon');
    grunt.loadNpmTasks('grunt-php');
    grunt.loadNpmTasks('grunt-browser-sync');

    // 初始化Grunt任務
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        // js語法檢查
        jshint: {
            all: {
                files: ['Gruntfile.js', '<%= pkg.sourcePath %>js/mc-main.js', '<%= pkg.sourcePath %>mobile-main.js', '<%= pkg.sourcePath %>pc-main.js']
            },
            mc: {
                files: ['Gruntfile.js', '<%= pkg.sourcePath %>js/mc-main.js']
            },
            pc: {
                files: ['Gruntfile.js', '<%= pkg.sourcePath %>js/pc-main.js']
            },
            mobile: {
                files: ['Gruntfile.js', '<%= pkg.sourcePath %>js/mobile-main.js']
            }
        },
        // javascript合併
        concat: { /*…*/
            pc: {
                options: {
                    banner: '/*! <%= grunt.template.today("yyyy-mm-dd") %> | <%= pkg.name %> | Pc | <%= pkg.description %> | <%= pkg.author %> */\n'
                },
                files: {
                    // '<%= pkg.webPath %>js/plugin/plugin.js': [
                    //     '<%= pkg.sourcePath %>js/plugin/jquery-1.11.0.min.js',
                    //     '<%= pkg.sourcePath %>js/plugin/jquery-ui-1.10.4.custom.min.js',
                    //     '<%= pkg.sourcePath %>js/plugin/modernizr.custom-2.8.3.js',
                    //     '<%= pkg.sourcePath %>js/plugin/pace.min.js',
                    //     '<%= pkg.sourcePath %>js/plugin/jquery.magnific-popup.min.js',
                    //     '<%= pkg.sourcePath %>js/plugin/validation-1.0.js',
                    //     '<%= pkg.sourcePath %>js/plugin/jquery.form.js',
                    //     '<%= pkg.sourcePath %>js/plugin/detect_browser.js',
                    //     '<%= pkg.sourcePath %>js/plugin/jquery.bxslider.js',
                    //     '<%= pkg.sourcePath %>js/plugin/share.js',
                    // ],
                    // '<%= pkg.webPath %>js/main.js': [
                    //     '<%= pkg.sourcePath %>js/pc-main.js'
                    // ],
                    // '<%= pkg.webPath %>js/plugin/ie8-fix.js': [
                    //     '<%= pkg.sourcePath %>js/plugin/respond.min.js',
                    //     '<%= pkg.sourcePath %>js/plugin/html5shiv.js'
                    // ],
                }
            },
        },
        // Compass SCSS
        compass: {
            pc: {
                options: {
                    config: '<%= pkg.sassPath %>pc-config.rb',
                    sassDir: '<%= pkg.sassPath %>pc-sass/',
                    cssDir: '<%= pkg.webPath %>__css/',
                    sourcemap: true,
                    outputStyle: 'compressed', // nested, expanded, compact, compressed.
                }
            },
            lightBox: {
                options: {
                    config: '<%= pkg.sassPath %>light-box-config.rb',
                    sassDir: '<%= pkg.sassPath %>light-box-sass/',
                    cssDir: '<%= pkg.webPath %>css/agenda/',
                    sourcemap: true,
                    outputStyle: 'compressed', // nested, expanded, compact, compressed.
                }
            },
            // mobile: {
            //     options: {
            //         config: '<%= pkg.sassPath %>mobile.config.rb',
            //         sassDir: '<%= pkg.sassPath %>mobile_sass/',
            //         cssDir: '<%= pkg.webPath %>manager/mobile/css/',
            //         sourcemap: true,
            //         outputStyle: 'expanded', // nested, expanded, compact, compressed.
            //     }
            // }
        },
        // 壓縮
        uglify: {
            pc: {
                options: {
                    banner: '/*! <%= grunt.template.today("yyyy-mm-dd") %> | <%= pkg.name %> | Pc | <%= pkg.description %> | <%= pkg.author %> */\n'
                },
                files: {
                    '<%= pkg.webPath %>js/main.js': ['<%= pkg.webPath %>js/main.js'],
                    '<%= pkg.webPath %>js/plugin/plugin.js': ['<%= pkg.webPath %>js/plugin/plugin.js'],
                    '<%= pkg.webPath %>js/plugin/ie8-fix.js': ['<%= pkg.webPath %>js/plugin/ie8-fix.js'],
                },
            },
            mobile: {
                options: {
                    banner: '/*! <%= grunt.template.today("yyyy-mm-dd") %> | <%= pkg.name %> | Mobile | <%= pkg.description %> | <%= pkg.author %> */\n'
                },
                files: {
                    'mobile/js/main.js': ['mobile/js/main.js'],
                },
            }
        },
        cssmin: {
            options: {
                sourceMap: true,
                target: '<%= pkg.webPath %>manager/css/plugin',
                shorthandCompacting: true,
                // roundingPrecision: -1
            },
            target: {
                files: {
                    // pc
                    '<%= pkg.webPath %>css/plugin/plugin.css': [
                        '<%= pkg.sourcePath %>css/plugin/ui-lightness/jquery-ui-1.10.4.custom.min.css',
                        '<%= pkg.sourcePath %>css/plugin/bxslider/jquery.bxslider.css',
                        '<%= pkg.sourcePath %>css/plugin/animate/animate.min.css',
                        '<%= pkg.sourcePath %>css/plugin/font-awesome/css/font-awesome.css',
                        '<%= pkg.sourcePath %>css/plugin/magnific-popup.css',
                        '<%= pkg.sourcePath %>css/plugin/cloud-zoom.css',
                        '<%= pkg.sourcePath %>css/plugin/flag-icon-css-master/css/flag-icon.min.css',
                    ],
                    // // Mobile
                    // '<%= pkg.webPath %>manager/mobile/css/plugin/plugin.css': [
                    //     '<%= pkg.sourcePath %>css/plugin/ui-lightness/jquery-ui-1.10.4.custom.min.css',
                    //     '<%= pkg.sourcePath %>css/plugin/animate/animate.min.css',
                    //     '<%= pkg.sourcePath %>css/plugin/bxslider/jquery.bxslider.css',
                    //     '<%= pkg.sourcePath %>css/plugin/font-awesome/css/font-awesome.min.css',
                    // ]
                }
            }
        },
        imagemin: {
            pc: { // Another target
                options: {
                    optimizationLevel: 3,
                },
                files: [{
                    expand: true, // Enable dynamic expansion
                    cwd: '<%= pkg.sourcePath %>images/', // Src matches are relative to this path
                    src: ['**/*.{png,jpg,gif,svg,PNG,JPG,GIF,SVG}'], // Actual patterns to match
                    dest: '<%= pkg.webPath %>images/' // Destination path prefix
                }]
            },
            mobile: {
                options: {
                    optimizationLevel: 3,
                },
                files: [{
                    expand: true, // Enable dynamic expansion
                    cwd: '<%= pkg.sourcePath %>mobile/images/', // Src matches are relative to this path
                    src: ['**/*.{png,jpg,gif,svg,PNG,JPG,GIF,SVG}'], // Actual patterns to match
                    dest: '<%= pkg.webPath %>mobile/images/' // Destination path prefix
                }]
            }
        },
        /* php */
        php: {
            dev: {
                options: {
                    hostname: '127.0.0.1',
                    port: 9000,
                    base: '../web', // Project root
                    keepalive: false,
                    open: false
                }
            }
        },
        browserSync: {
            dev: {
                bsFiles: {
                    src : [
                        '<%= pkg.webPath %>css/**/*.css',
                        '<%= pkg.webPath %>**/*.html',
                    ]
                },
                options: {
                    proxy: '<%= php.dev.options.hostname %>:<%= php.dev.options.port %>',
                    watchTask: true,
                    // notify: true,
                    // open: true,
                    // logLevel: 'silent',
                    port: 8080,
                    ui:{
                        port: 8081,
                    },
                    // ghostMode: {
                    //     clicks: true,
                    //     scroll: true,
                    //     links: true,
                    //     forms: true
                    // },
                }
            }
        },
        /* static server */
        // browserSync: {
        //     dev: {
        //         bsFiles: {
        //             src : [
        //                 '<%= pkg.webPath %>css/**/*.css',
        //                 '<%= pkg.webPath %>**/*.html',
        //             ]
        //         },
        //         options: {
        //             watchTask: true,
        //             server: '../web',
        //             port: 8080,
        //             ui:{
        //                 port: 8181,
        //             },
        //         }
        //     }
        // },
        watch: {
            options: {
                // reload: true,
                // livereload: true,
                spawn: false,
            },
            compass_pc: {
                files: '<%= pkg.sassPath %>pc-sass/**/*.scss',
               tasks: ['compass:pc']
            },
            // compass_mobile: {
            //     files: '<%= pkg.sassPath %>mobile_sass/**/*.scss',
            //     tasks: ['compass:mobile']
            // },
            concat: {
                files: '<%= jshint.pc.files %>',
                tasks: ['concat:pc']
            },
            // php: {
            //     files: ['<%= pkg.webPath %>manager/**/*.php', '<%= pkg.webPath %>**/*.php'],
            //     task: ['php']
            // }
        },
        realFavicon: {
            favicons: {
                src: '<%= pkg.sourcePath %>images/favicon.png',
                dest: '<%= pkg.webPath %>images/',
                options: {
                    iconsPath: 'images/',
                    html: [ '<%= pkg.webPath %>images/favicons.html' ],
                    design: {
                        ios: {
                            pictureAspect: 'noChange'
                        },
                        desktopBrowser: {},
                        windows: {
                            pictureAspect: 'noChange',
                            backgroundColor: '#FFFFFF',
                            onConflict: 'override'
                        },
                        androidChrome: {
                            pictureAspect: 'noChange',
                            themeColor: '#FFFFFF',
                            manifest: {
                                name: '<%= pkg.name %>',
                                display: 'browser',
                                orientation: 'notSet',
                                onConflict: 'override',
                                declared: true
                            }
                        },
                        safariPinnedTab: {
                            pictureAspect: 'silhouette',
                            themeColor: '#FFFFFF'
                        }
                    },
                    settings: {
                        scalingAlgorithm: 'Mitchell',
                        errorOnImageTooSmall: false
                    }
                }
            }
        }
    });

    // 註冊一個任務，第二參數可以是數字或者字串
    // 預設執行default任務.
    // grunt.registerTask('default', ['jshint', 'concat', 'uglify', 'compass']);
    grunt.registerTask('default', ['php:dev','browserSync:dev','watch']);
    // grunt.registerTask('default', ['browserSync','watch']);
    // All
    // grunt.registerTask('build', ['uglify', 'imagemin', 'cssmin']);
    // Pc
    grunt.registerTask('build', ['uglify:pc', 'cssmin']);
    // Mobile
    // grunt.registerTask('build', ['uglify:mobile', 'imagemin:mobile', 'cssmin:mobile']);
};
