### Grunt File Sample
- path:  
    compass/  
    ----    pc-sass/  
    ----    pc-config.rb  
    grunt/  
    ----    Gruntfile.js  
    ----    package.json  
    source/  
    ----    css/  
    ----    images/  
    ----    js/  
    web/  
- run `npm install`
- run `grunt`

Rock on!